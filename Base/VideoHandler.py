################################################################################
######################### RCP Wheeling Video Analytics #########################
################################################################################

################################################################################
# Copyright @ 2019 by Deloitte, All rights reserved
#
# This software is proprietary to and embodies the confidential technology
# of Deloitte. Possession, use, or copying of this software and media is
# authorized only pursuant to a valid written license from Deloitte or
# an authorized sublicensor.
#
# File:         VideoHandler.py
# Author:       Ankur Shukla
# Created On:   April 24, 2019
# Modified On:  May 23, 2019
#
# Implementation of Video Analytics pipeline to provide easy to use attributes
# and methods to process videos and generate log files fit for further analysis
# The implementation is in the form of a class which inherites VideoCapture class
# from OpenCV

# References:
# https://docs.opencv.org/3.4.3/d8/dfe/classcv_1_1VideoCapture.html
#
################################################################################


# Importing required Python packages
import cv2
import math
import queue
import pandas as pd
from tqdm import tqdm
import matplotlib.pyplot as plt
from aws.s3 import upload_file


# Custom queue class defined to store machine state history. This queue has a custom put function
# When a new element is pushed in the queue when it is full the oldest element is poped out and
# the new element is then pushed.
# Attributes:
# - maxsize = maximum size of the queue in seconds. Default value is 5 seconds
class FrameQueue(queue.Queue):
    def __init__(self, maxsize=5):
        super().__init__(maxsize)

    def put_(self, element):
        if self.full():
            self.get()
            self.put(element)
        else:
            self.put(element)

    def size(self):
        return len(list(self.queue))


# VideoHandler class inherits VideoCapture class from OpenCV.
# This class provides all the attributes and methods of the VideoCapture class

# Attributes: there are some mandatory arguments for this class which are listed below.
# Apart from those, user can add custom attributes as per their need.
# - path = path to the video file
# - df = Pandas dataframe for storing video analytics
# - cols = contain the list of columns in "df"
# - filename = filename for the video whose path is in path variable
# - framesProcessed = global counter of number of frames which have been processed
# by the processFrames function
# - bkgrnd = stores background information for each frame by
# using createBackgroundSubtractorMOG function.
# Read https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_video/py_bg_subtraction/py_bg_subtraction.html
# for more details on background subtraction
# - secPerFrame = time interval between two frames of a videos
# - startFrame = starting point of the chunk of video to be analyzed
# - endFrame = ending point of the chunk of video to be analyzed
class VideoHandler(cv2.VideoCapture):
    # Parameterized constructor having 'path' (input video path) as the compulsory argument.
    # This takes variable number of arguments and creates class attributes using them.
    # Apart from them it also creates other attributes including a dataframe
    # for frame wise logging.
    def __init__(self, **kwargs):
        if 'path' not in kwargs.keys():
            raise ValueError('Path to video file not specified')
        else:
            super().__init__(kwargs['path'])
            self.__dict__.update(kwargs)
            self.cols = []
            self.fileName = self.path.split('.')[-2].split('/')[-1]
            self.writer = None
            self.framesProcessed = 0
            self.df = pd.DataFrame(columns=['frame'])
            self.bkgrnd = cv2.bgsegm.createBackgroundSubtractorMOG()
            self.secPerFrame = 1 / math.ceil(self.get(cv2.CAP_PROP_FPS))
            if 'windows' in kwargs.keys():
                for win in kwargs['windows'].keys():
                    self.__dict__[win] = FrameQueue(maxsize=kwargs['windows'][win] * int(self.get(cv2.CAP_PROP_FPS)))

    # Method to play complete video in one go without any processing
    def showVideo(self):
        counter = self.startFrame
        while self.isOpened():
            ret, frame = self.read()
            if ret is True and counter < self.endFrame:
                cv2.imshow('frame', frame)
                counter += 1
                # & 0xFF is required for a 64-bit system
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            else:
                break
                # continue
        cv2.destroyAllWindows()

    # Method to add custom columns to class dataframe.
    # Arguments:
    # - cols = dictionary containing column names as keys and column defaults as values
    def addColumnsToDF(self, cols):
        for c in cols.keys():
            self.df[c] = cols[c]
        self.cols = self.df.columns.tolist()

    # Method to initialize OpenCV video wirter object for saving videos to disk.
    # Arguments:
    # - output_path = path to store the output video
    # - codec = codec used to intialize video writer object
    # - isColor = if set the video is saved in color
    # Read https://docs.opencv.org/3.4.3/dd/d9e/classcv_1_1VideoWriter.html for more details
    def setVideoWriter(self, output_path=None, codec='XVID', isColor=True):
        print('Warning: please look for codecs and file extension best suitable for your system')
        if output_path is None:
            output_path = 'output.' + self.path.split('.')[-1]
        fourcc = cv2.VideoWriter_fourcc(*codec)
        writer = cv2.VideoWriter(output_path, fourcc, int(self.get(cv2.CAP_PROP_FPS)),
                                 (int(self.get(cv2.CAP_PROP_FRAME_WIDTH)),
                                  int(self.get(cv2.CAP_PROP_FRAME_HEIGHT))), isColor)

        self.writer = writer

    # Function to allow user to create ROIs and get them printed in graphical user interface
    def getROI(self):
        ret, fr = self.read()
        bboxes = []
        bid = 0
        if ret:
            while True:
                cv2.imshow('Select BBoxes', fr)
                if cv2.waitKey(1) & 0xFF == ord('s'):
                    bid += 1
                    bbox = cv2.selectROI('Select BBoxes', fr, showCrosshair=False)
                    cv2.rectangle(fr, (bbox[0], bbox[1]), (bbox[2] + bbox[0], bbox[3] + bbox[1]), (255, 255, 255), 1)
                    # xmid = (bbox[0] + bbox[2]) / 2
                    # ymid = (bbox[1] + bbox[3]) / 2
                    # cv2.putText(fr, 'BBox {}: {}'.format(str(bid), str(bbox)), (int(xmid), int(ymid)),
                    # cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2, lineType=cv2.LINE_AA)
                    bboxes.append(bbox)
                    continue
                elif cv2.waitKey(1) & 0xFF == ord('n'):
                    ret, fr = self.read()
                    if ret:
                        continue
                elif cv2.waitKey(1) & 0xFF == ord('q'):
                    cv2.destroyAllWindows()
                    break
        print('BBoxes in format (x1, y1, width, height)')
        print(bboxes)
        plt.imshow(fr)
        plt.show()

    # Method to perform custom operations on video frames and display/save them.
    # Arguments:
    # - display = True if the processed video should also be played during processing
    # save_video = True if the video is to be saved to disk. setVideoWriter method
    # should have been called to be able to save videos
    # csv = True if the analytics dataframe has to be saved to disk
    # func = custom function defined by the user for processing frames which should return
    # a valid frame and a dictionary having column names as keys and row values as values
    # debug = User can use this flag to print additional log in their custom function
    # kwargs = variable number of arguments needed for custom user function
    def processFrames(self, display=True, save_video=False,
                      csv=False, func=None, debug=False, **kwargs):
        write = 0
        counter = 0
        if save_video:
            if self.writer is None:
                print('No writer configured for saving frames. Not writing the video to disk')
            else:
                write = 1
        pbar = tqdm(total=self.get(cv2.CAP_PROP_FRAME_COUNT))
        while self.isOpened():
            counter += 1
            ret, frame = self.read()
            if ret and self.framesProcessed + counter <= self.get(cv2.CAP_PROP_FRAME_COUNT):
                if func is None:
                    f = frame
                    self.df = self.df.append({'frame': self.framesProcessed + counter},
                                             ignore_index=True, sort=False)
                else:
                    f, row = func(self, frame, counter, debug, display, kwargs)
                    self.df = self.df.append(row, ignore_index=True, sort=False)

                if write:
                    self.writer.write(f)

                if display:
                    cv2.imshow('frame', f)
                    # & 0xFF is required for a 64-bit system
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
                pbar.update(1)
            elif not ret and self.framesProcessed + counter <= self.get(cv2.CAP_PROP_FRAME_COUNT):
                continue
            else:
                break
        pbar.close()
        self.df = self.df.append({'frame': self.get(cv2.CAP_PROP_FRAME_COUNT),
                                  'pressCount': self.presses,
                                  'machineState': self.state,
                                  'presses_per_minute': self.avg_pressesperminute}, ignore_index=True, sort=False)
        if write:
            self.writer.release()
            print('Write complete')
        if display:
            cv2.destroyAllWindows()
        temp = self.framesProcessed
        self.framesProcessed += counter

        self.df['seconds'] = self.df['frame'] * self.secPerFrame
        if csv:
            # self.df.loc[temp:self.framesProcessed].to_csv('output_log_{}_{}.csv'.format(self.fileName,
            #                                                                             self.process))
            csv_file_name = 'output_log_{}.csv'.format(self.fileName)
            # self.df.loc[temp:self.framesProcessed].to_csv('output_log_{}.csv'.format(self.fileName))
            self.df.loc[temp:self.framesProcessed].to_csv(csv_file_name, encoding='utf-8')

            # upload output to S3
            upload_file(csv_file_name)

        return self.df.loc[temp:self.framesProcessed]
