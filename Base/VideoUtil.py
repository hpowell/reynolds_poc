################################################################################
######################### RCP Wheeling Video Analytics #########################
################################################################################

################################################################################
# Copyright @ 2019 by Deloitte, All rights reserved
#
# This software is proprietary to and embodies the confidential technology
# of Deloitte. Possession, use, or copying of this software and media is
# authorized only pursuant to a valid written license from Deloitte or
# an authorized sublicensor.
#
# File:         VideoUtil.py
# Author:       Ankur Shukla
# Created On:   April 29, 2019
# Modified On:  May 23, 2019
#
# Set of convenience functions to abstract the implementation of commonly used
# image processing functions such as morphological operations.
#
################################################################################

# Importing required Python packages
import cv2
import imutils
import numpy as np
import matplotlib.pyplot as plt


# Function to implement morphological opening
# Arguments:
# - frame = frame on which opening has to be performed. Binary frame is expected
# - kernel = kernel used to perform morphological operations
# - erode = number of iterations for eroding
# - dilate = number of iterations for dilation
def morphOpening(frame, kernel=np.ones((2, 2), np.uint8), erode=1, dilate=1):
    return cv2.dilate(cv2.erode(frame, kernel, iterations=erode), kernel, iterations=dilate)


# Convenience function for fitting circle on given ROI
# Arguments:
# - roi = cropped area of frame containing blob on which circle is to be fitted.
# - thl = lower threshold for valid circle radius
# - thu = upper threshold for valid circle radius
def fitCircle(roi, thl=1, thu=100):
    cnts = cv2.findContours(roi, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    results = []
    if cnts is not None:
        if len(cnts) > 0:
            for c in cnts:
                ((x, y), radius) = cv2.minEnclosingCircle(c)
                cv2.moments(c)
                if thl < radius <= thu:
                    results.append((x, y, radius))
    return results


# Returns a array with given ROI
# Arguments:
# - frame = valid video frame
# - x1 = upper left corner x coordinate
# - y1 = upper left corner y coordinate
# - x2 = lower right corner x coordinate
# - y2 = lower right corner y coordinate
def getMask(frame, x1, y1, x2, y2):
    mask = np.zeros(frame.shape[:2], dtype="uint8")
    cv2.rectangle(mask, (x1, y1), (x2, y2), (255, 255, 255), -1)
    crop = cv2.bitwise_and(frame, frame, mask=mask)

    return mask, crop


# Write text to a frmae
# Arguments:
# - frame = valid video frame
# - string = string to be written to the frame
# - pos = position of the text in frame (x, y)
# - scale = scale factor for text size
# - color = text color in RGB space
# - thickness = text thickness
def writeText(frame, string, pos=(0, 0), scale=1, color=(255, 255, 255), thickness=2):
    cv2.putText(frame, string, pos,
                cv2.FONT_HERSHEY_SIMPLEX, scale, color, thickness, lineType=cv2.LINE_AA)


# Plot given frame using matplotlib
# Arguments:
# frame = valid video frame
def plotFrame(frame):
    plt.imshow(frame)
    plt.show()


# Function to calculate IOU for given two bounding boxes
# Arguments:
# -boxA: bounding box 1 [x1, y1, x2, y2]
def getIOU(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou


# Function to get IOU for one ROI with multiple ROIs
# Arguments:
# - roi = bounding box of the roi
# - bboxes = list of bounding boxes for which IOU is desired with respect to roi
def compIOU(roi, bboxes):
    ious = []
    for box in bboxes:
        ious.append(round(getIOU(roi, [box[0][0], box[0][1],
                                       box[0][0] + box[0][2], box[0][1] + box[0][3]]), 2))

    return sum(ious)
