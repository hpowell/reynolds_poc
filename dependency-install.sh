#!/bin/bash

set -ex

echo "APT update and upgrade"
sudo apt update -y
sudo apt upgrade -y

echo "APT installs"
sudo apt install -y git curl
sudo apt install -y python3.7-dev python3-pip
sudo apt install -y python-gst-1.0 python3-gst-1.0
sudo apt install -y libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev ffmpeg libavutil-dev libavcodec-dev libavformat-dev
sudo apt install -y libcanberra-gtk-module
sudo apt install -y libsm6 libxext6

echo "PIP installs"
sudo python3.7 -m pip install boto3==1.9.232
sudo python3.7 -m pip install keyboard==0.13.3
sudo python3.7 -m pip install matplotlib==3.1.1
sudo python3.7 -m pip install numpy==1.17.2
sudo python3.7 -m pip install opencv-python==4.1.1.26
sudo python3.7 -m pip install opencv-contrib-python==4.1.1.26
sudo python3.7 -m pip install pandas==0.25.1
sudo python3.7 -m pip install tqdm==4.36.1
sudo python3.7 -m pip install imutils==0.5.3
