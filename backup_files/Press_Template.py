################################################################################
######################### RCP Wheeling Video Analytics #########################
################################################################################

################################################################################
# Copyright @ 2019 by Deloitte, All rights reserved
#
# This software is proprietary to and embodies the confidential technology
# of Deloitte. Possession, use, or copying of this software and media is
# authorized only pursuant to a valid written license from Deloitte or
# an authorized sublicensor.
#
# File:         Press_Template.py
# Author:       Ankur Shukla
# Created On:   May 23, 2019
# Modified On:  May 23, 2019
#
# Template code for creating VideoHandler object and calling the processFrames
# function with custom made trim_press function to implement press analytics
# for production count
#
################################################################################

# Importing requires Python packages
import sys
sys.path.append('../')
import cv2
import numpy as np
import Base.VideoUtil as vu
from Base.VideoHandler import VideoHandler

# Creating a new VideoHandler object
vs = VideoHandler(path='path/to/your/video',
                  presses=0,
                  windows={'watchWindow': seconds, 'blockWindow1': seconds, 'blockWindow2': seconds},
                  flag='UP',
                  state='OFF',
                  tracker=cv2.MultiTracker_create(), avg_pressesperminute=0)

# Creating video writer object
vs.setVideoWriter()

# Adding custom columns to store plate count and machine state
vs.addColumnsToDF({'dictionary of your columns and default values here'})

# Reading the first frame
ret, fr = vs.read()

# Processing the first frame of the video to extract the machine body and
# the area where the plates are pushed out of the trim press
curr_frame = cv2.cvtColor(fr, cv2.COLOR_BGR2RGB)

# Defining ROIs for press area, left chute (chute 1) and right chute (chute 2)
o_roi = [x1, y1, x2, y2]
i_roi = [x1, y1, w, h]

bowl_mask_1, plate1 = vu.getMask(curr_frame, x1, y1, x2, y2)
bowl_mask_2, plate2 = vu.getMask(curr_frame, x1, y1, x2, y2)
half_line_mask1, half_line1 = vu.getMask(curr_frame, x1, y1, x2, y2)
half_line_mask2, half_line2 = vu.getMask(curr_frame, x1, y1, x2, y2)
full_line_mask1, full_line1 = vu.getMask(curr_frame, x1, y1, x2, y2)
full_line_mask2, full_line2 = vu.getMask(curr_frame, x1, y1, x2, y2)


# Initializing tracker to track press movement
vs.tracker.add(cv2.TrackerBoosting_create(), fr[o_roi[1]:o_roi[3], o_roi[0]:o_roi[2]], (i_roi[0], i_roi[1], i_roi[2], i_roi[3]))


# Method to perform trim press analytics
# Takes new frame, index of frame being processed and variable number of arguments.
# Arguments:
# - frame = video frame
# - counter = local counter to track number of frames processed in one function call
def trim_press(self, frame, counter, debug, display, kwargs):
    row = None

    # Updating the state queue based on machine and plates flags
    if self.framesProcessed+counter == 0:
        state = False
        self.state = False
        self.df = self.df.append({'frame': 0,
                                  'pressCount': self.presses,
                                  'machineState': False,
                                  'leftChute': 'Free',
                                  'rightChute': 'Free',
                                  'half_tray1': False,
                                  'half_tray2': False,
                                  'full_tray1': False,
                                  'full_tray2': False}, ignore_index=True, sort=False)
    else:
        state = any(self.watchWindow.queue)

    # If state is ON and the press moves up after moving down then increment press count
    _, box = self.tracker.update(frame[kwargs['press_area'][1]:kwargs['press_area'][3],
                                       kwargs['press_area'][0]:kwargs['press_area'][2]])
    y = box[0][1] + box[0][3]/2
    self.watchWindow.put_(y > kwargs['state_th'])

    # Checking for plate ROI intensities
    avg_chute1 = np.average(cv2.bitwise_and(frame, frame, mask=kwargs['plates_area1']))
    avg_chute2 = np.average(cv2.bitwise_and(frame, frame, mask=kwargs['plates_area2']))

    self.blockWindow1.put_(avg_chute1 > kwargs['block_th1'])
    self.blockWindow2.put_(avg_chute2 > kwargs['block_th2'])

    # Checking for press status and updating count
    if y > kwargs['state_th'] and self.flag == 'UP' and state:
        cv2.line(frame, (kwargs['line_x'], kwargs['line_y']),
                 (kwargs['line_w'] + kwargs['line_x'], kwargs['line_y']), (0, 255, 0), thickness=3, lineType=8, shift=0)

        self.presses += 1
        self.flag = 'DOWN'
    elif y < kwargs['press_th'] and self.flag == 'DOWN':
        self.flag = 'UP'

    self.avg_pressesperminute = round((self.presses*60)/((self.framesProcessed+counter)*self.secPerFrame), 2)
    chute1 = 'Free'
    chute2 = 'Free'

    # Append log data to the dataframe object
    if state != self.state:
        if all(self.blockWindow1.queue):
            chute1 = 'Clogged'
        elif not any(self.blockWindow1.queue) and state and self.blockWindow1.size() == self.windows['blockWindow1']:
            chute1 = 'Stuck'

        if all(self.blockWindow2.queue):
            chute2 = 'Clogged'
        elif not any(self.blockWindow2.queue) and state and self.blockWindow2.size() == self.windows['blockWindow2']:
            chute2 = 'Stuck'

        orange_roi = cv2.inRange(frame, (175, 31, 0), (255, 255, 255))
        half_tray1 = cv2.bitwise_and(orange_roi, orange_roi, mask=kwargs['half_lineroi1'])
        half_tray2 = cv2.bitwise_and(orange_roi, orange_roi, mask=kwargs['half_lineroi2'])
        half_tray1 = np.average(half_tray1) > kwargs['tray_th']
        half_tray2 = np.average(half_tray2) > kwargs['tray_th']
        full_tray1 = cv2.bitwise_and(orange_roi, orange_roi, mask=kwargs['full_lineroi1'])
        full_tray2 = cv2.bitwise_and(orange_roi, orange_roi, mask=kwargs['full_lineroi2'])
        full_tray1 = np.average(full_tray1) > kwargs['tray_th']
        full_tray2 = np.average(full_tray2) > kwargs['tray_th']

        self.state = state
        if debug:
            row = {'frame': self.framesProcessed+counter,
                   'pressCount': self.presses,
                   'machineState': state,
                   'presses_per_minute': self.avg_pressesperminute,
                   'leftChute': chute1,
                   'rightChute': chute2,
                   'half_tray1': half_tray1,
                   'half_tray2': half_tray2,
                   'full_tray1': full_tray1,
                   'full_tray2': full_tray2}
        else:
            row = {'frame': self.framesProcessed+counter,
                   'pressCount': self.presses,
                   'machineState': state,
                   'presses_per_minute': self.avg_pressesperminute,
                   'leftChute': chute1,
                   'rightChute': chute2,
                   'half_tray1': half_tray1,
                   'half_tray2': half_tray2,
                   'full_tray1': full_tray1,
                   'full_tray2': full_tray2}

    if display:
        # Write text on video frame
        fr = frame.copy()
        cv2.rectangle(fr, (0, 0), (200, 130), (255, 255, 255), cv2.FILLED)
        vu.writeText(fr, 'Avg Presses/Min {} '.format(str(y), str(self.avg_pressesperminute), ),
                     pos=(0, 30), scale=0.5, color=(0, 0, 255))
        vu.writeText(fr, 'Left Chute: {}'.format(chute1), pos=(0, 60), scale=0.5, color=(0, 0, 255))
        vu.writeText(fr, 'Right Chute: {}'.format(chute2), pos=(0, 90), scale=0.5, color=(0, 0, 255))
        vu.writeText(fr, 'M/C State ', pos=(0, 120), scale=0.5, color=(0, 0, 255))

        if state:
            vu.writeText(fr, ' ON', pos=(85, 120), scale=0.5, color=(0, 0, 255))
        else:
            vu.writeText(fr, ' OFF', pos=(85, 120), scale=0.5, color=(0, 0, 255))

        cv2.addWeighted(fr, 0.6, frame, 0.4, 0, frame)

    # Returning frame
    return frame, row


# Calling the processing function the following are the arguments:
# 1. func = the function to be called for processing each frame
# 2. press_area = roi the press area
# 3. bar_area = roi containing the black slit on the press
# 4. plates_area1 = roi containg left chute
# 5. plates_area2 = roi containg right chute
# 6. press_th = press threshold below which the press is considered to be in UP position
# 7. state_th = state threshold above which the press is considered to be in DOWN position
# 8. block_th1 = threshold above which left chute is considered to be blocked
# 9. block_th2 = threshold above which right chute is considered to be blocked
# 10. bowl_gap = gap in seconds between bowl output from one chute
# 11. line_x = left x coordinate for blip line for each press
# 12. line_y = y coordinate for blip line for each press
# 13. line_w = width of blip line for each press
# 14. half_lineroi1 = roi for half fill mark on left track
# 15. half_lineroi2 = roi for half fill mark on right track
# 16. full_lineroi1 = roi for full mark on left track
# 15. full_lineroi2 = roi for full mark on right track
# 16. debug = flag to enable debugging information logging
# 17. save_video = flag to save video to disk
# 18. display = if True the video is displayed while being processed
# 19. csv = flag to write csv output to disk
# 20. tray_th = threshold to check whether plates have covered a track ROI
vs.processFrames(func=trim_press,
                 press_area=o_roi,
                 bar_area=i_roi,
                 plates_area1=bowl_mask_1,
                 plates_area2=bowl_mask_2,
                 press_th=lower_threshold,
                 state_th=upper_threshold,
                 block_th1=left_chute_block_threshold,
                 block_th2=right_chute_block_threshold,
                 bowl_gap=seconds_between_two_chute_outputs,
                 line_x=left_x_for_blip_line,
                 line_y=left_y_of_blip_line,
                 line_w=width_of_blip_line,
                 half_lineroi1=half_line_mask1,
                 half_lineroi2=half_line_mask2,
                 full_lineroi1=full_line_mask1,
                 full_lineroi2=full_line_mask2,
                 tray_th=tray_threshold,
                 debug=debug_flag,
                 save_video=save_video_flag,
                 display=display_flag,
                 csv=save_csv_flag)
