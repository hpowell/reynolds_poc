import boto3

BUCKET_NAME = 'reynolds-poc-dev'
# Key = 'output_log_5_min_safe_crop.csv'
# output_name = 'output_log_5_min_safe_crop.csv'


def upload_file(key_name):
    s3 = boto3.client('s3')

    output_name = key_name
    s3.upload_file(key_name, BUCKET_NAME, output_name)
