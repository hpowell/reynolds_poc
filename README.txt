################################################################################
######################### RCP Wheeling Video Analytics #########################
################################################################################

################################################################################
# Copyright @ 2019 by Deloitte, All rights reserved
#
# This software is proprietary to and embodies the confidential technology
# of Deloitte. Possession, use, or copying of this software and media is
# authorized only pursuant to a valid written license from Deloitte or
# an authorized sublicensor.
#
# File:         README.txt
# Author:       Ankur Shukla
# Created On:   May 23, 2019
# Modified On:  May 23, 2019
#
# Readme file for RCP Wheeling Video Analytics code base.
#
################################################################################

########################## Video Processing Pipeline ###########################

The video processing pipeline consists of a VideoHandler class which is a child
class of VideoCapture class from OpenCV. This class provides the ability of defining
custom global attributes required to process videos for a user defined use case.
This class also provides some methods which can be invoked for different functionalities.
The following are the methods and their description:

1. __init__:
A parameterized constructor which takes the video path as mandatory argument along
with a variable set of arguments (kwargs).

2. showVideo:
A method to play the video from start to end

3. addColumnsToDF:
A method to add columns to the log dataframe based on user requirement

4. setVideoWriter:
Method to instantiate VideoWriter object from OpenCV which can be used to save
video output to a local drive

5. getROI:
A convenient method to display the first frame of the video and select multiple
ROIs. To select new ROI, the user needs to press 's' key and then draw a bounding box
on the frame and then press 'ENTER'. When finished the user needs to press 'q'
to exit the selection window and print the list of bounding boxes along with a plot
of the frame with the bounding box on screen.

6. processFrames:
This method enables the user to tie up a custom frame processing function to the
VideoHandler object. This method takes a function as argument (func) which is
called to process the frames. This function should return a processed frame and dictionary
with valid column names and row values which are added to the exported dataframe.
This method also takes in arguments such as display, save_video, csv, debug to enable
the user to toggle some input output options. Refer to the VideoHandler file for more
details on the arguments.

############################ Press Production Count ############################

The press production count template file (Press_Template.py) file needs to be tuned to a particular video to
process it. The file does the following:

1. Imports the required Python packages
2. Instantiates a VideoHandler object with requires attributes
3. Instantiates a VideoWriter object using setVideoWriter method
4. Adds the required columns to the analysis dataframe
5. Initializes the following ROIs needed for press analysis:
   - o_roi = outer ROI for press
   - i_roi = inner ROI containing the black slit on the press
   - bowl_mask_1 = ROI containing left chute
   - bowl_mask_2 = ROI containing right chute
   - half_line_mask1 = ROI containing half full mark on left track
   - half_line_mask2 = ROI containing half full mark on right track
   - full_line_mask1 = ROI containing full mark on left track
   - full_line_mask2 = ROI containing full mark on right track
6. Instantiates a boosting tracker using OpenCV. Ref: https://docs.opencv.org/3.4/d9/df8/group__tracking.html
7. Defines a trim_press function which is tied up to the VideoHandler class to process
the incoming video frames. Refer the Press_Template file for details on this function.
8. Calls the processFrames function with trim_press as the func value along with ROI and
threshold values for a particular video
